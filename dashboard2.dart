import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:philile_hlalukana_module5/screens/welcome.dart';
import 'package:philile_hlalukana_module5/screens/user_profile_edit.dart';
import 'home_chillers.dart';


class Dashboardd extends StatefulWidget {
  static const routeName = '/dashboardd';

  const Dashboardd({Key? key}) : super(key: key);

  @override
  State<Dashboardd> createState() => _DashboarddState();
}

class _DashboarddState extends State<Dashboardd> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Scaffold(

          appBar: AppBar(
            title: const Text('Dashboard'),
          ),
          
          body: SafeArea(
            child: Flexible(
              child: GridView.count(
                childAspectRatio: 1.2,
                padding: const EdgeInsets.only(left: 16, right: 16, top: 16.0),
                crossAxisCount: 2,
                crossAxisSpacing: 40,
                mainAxisSpacing: 40,
                shrinkWrap: true,
                primary: true,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => const HomeChillersPage()));
                      if (kDebugMode) {
                        print("Create an Account");
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image:  DecorationImage(
                          fit: BoxFit.fill,
                          colorFilter: ColorFilter.mode(Colors.blueGrey.withOpacity(0.5), BlendMode.dstATop),
                          image: const NetworkImage(
    "https://media.istockphoto.com/videos/asian-woman-having-virtual-party-in-vr-glasses-at-home-video-id1316991031?s=640x640"),

                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          SizedBox(height: 14),
                          Text(
                              'VR Entertainment',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 40,
                                fontWeight: FontWeight.bold,)),


                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (kDebugMode) {
                        print("Access Qr Services");
                      }
                    },
                    child: Container(

                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image:  DecorationImage(
                        fit: BoxFit.fill,
                        colorFilter: ColorFilter.mode(Colors.blueGrey.withOpacity(0.5), BlendMode.dstATop),
                        image: const NetworkImage(
                            "https://ibb.co/bHxGJn6"),

                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const <Widget>[
                        SizedBox(height: 14),
                        Text(
                            'Qr Access',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,)),

                      ],
                    ),
                  ),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (kDebugMode) {
                        print("Tickets");
                      }
                    },
                    child: Container(

                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image:  DecorationImage(
                          fit: BoxFit.fill,
                          colorFilter: ColorFilter.mode(Colors.blueGrey.withOpacity(0.5), BlendMode.dstATop),
                          image: const NetworkImage(
                              "https://miro.medium.com/max/1838/0*da56Kwxh3OaO4Gm6"),

                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          SizedBox(height: 14),
                          Text(
                              'Tickets Services',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,)),

                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (kDebugMode) {
                        print("Transactions Payments & Balance");
                      }
                    },
                    child: Container(

                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          colorFilter: ColorFilter.mode(Colors.blueGrey.withOpacity(0.5), BlendMode.dstATop),
                          image: const NetworkImage(
                              "https://ibb.co/61LZnsM"),

                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          SizedBox(height: 14),
                          Text(
                              'Account',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,)),

                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (kDebugMode) {
                        print("Contact Us, Messages & Help");
                      }
                    },
                    child: Container(

                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          colorFilter: ColorFilter.mode(Colors.blueGrey.withOpacity(0.5), BlendMode.dstATop),
                          image: const NetworkImage(
                              "https://ibb.co/0hHVTk2"),

                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          SizedBox(height: 14),
                          Text(
                              'Communication',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,)),

                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (kDebugMode) {
                        print("Promotions & Rewords");
                      }
                    },
                    child: Container(

                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image:  DecorationImage(
                          fit: BoxFit.fill,
                          colorFilter: ColorFilter.mode(Colors.blueGrey.withOpacity(0.5), BlendMode.dstATop),
                          image: const NetworkImage(
                              "https://caboodle-technology.co.uk/wp-content/uploads/2019/03/10-Best-Non-Monetary-Rewards.jpeg"),

                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          SizedBox(height: 14),
                          Text(
                              'Rewards',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,)),

                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(

            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (
                  ctx) =>  const ProfilePage()));
            },

            tooltip: 'View Profile',
            child: const Icon(Icons.person),

          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        ),

      ],
    );
  }


}
