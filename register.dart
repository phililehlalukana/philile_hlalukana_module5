import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:philile_hlalukana_module5/screens/login.dart';
import 'dashboard2.dart';


class SignupScreen extends StatelessWidget {

  static const routeName = '/signup-screen';

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final userNameController = TextEditingController();

  SignupScreen({Key? key}) : super(key: key);

  Widget signUpWith(IconData icon) {
    return Container(
      height: 50,
      width: 115,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey, width: 1),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, size: 24),
          TextButton(onPressed: () {}, child: const Text('Sign in')),
        ],
      ),
    );
  }
  Widget userInput(TextEditingController userInput, String hintTitle, TextInputType keyboardType) {
    return Container(
      height: 55,
      margin: const EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(color: Colors.blueGrey.shade200, borderRadius: BorderRadius.circular(30)),
      child: Padding(
        padding: const EdgeInsets.only(left: 25.0, top: 15, right: 25),
        child: TextField(
          controller: userInput,
          autocorrect: false,
          enableSuggestions: false,
          autofocus: false,
          decoration: InputDecoration.collapsed(
            hintText: hintTitle,
            hintStyle: const TextStyle(fontSize: 18, color: Colors.white70, fontStyle: FontStyle.italic),
          ),
          keyboardType: keyboardType,
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

        Container(
        height: 500,
        width: double.maxFinite,
        decoration: const BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
    ),

    child: Padding(
    padding: const EdgeInsets.all(5.0),
    child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
    const SizedBox(height: 25),
    userInput(emailController, 'Email', TextInputType.emailAddress),
    userInput(passwordController, 'Password', TextInputType.visiblePassword),
    userInput(firstNameController, 'Firstname', TextInputType.text),
    userInput(lastNameController, 'Lastname', TextInputType.text),
    userInput(userNameController, 'Username', TextInputType.text),
    SizedBox(
    height: 35,
    child: ElevatedButton(
    onPressed: () {
    if (kDebugMode) {
    print(firstNameController);
    }
    if (kDebugMode) {
    print(lastNameController);
    }
    if (kDebugMode) {
    print(userNameController);
    }
    if (kDebugMode) {
    print(emailController);
    }
    if (kDebugMode) {
    print(passwordController);
    }
    Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => const Dashboardd()));
    },
    child: const Text('Sign up', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.white,),),
    ),
    ),
    const SizedBox(height: 20),
    const Center(child: Text('already have an account? '
    'Click below'),

    ),

    ],
    ),
    ),
    ),
          Container(
            height: 35,

            padding: const EdgeInsets.only(top: 5, left: 70, right: 70),
            child:ElevatedButton(
                child :  const Text(
                  'Login Page',
                  style: TextStyle(fontSize: 15),
                ),
                onPressed: () {

                  Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => LoginScreen()));}
            ),
          ),
        ],
        ),
    );
  }
}
