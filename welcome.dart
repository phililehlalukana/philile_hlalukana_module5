import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:philile_hlalukana_module5/screens/register.dart';
import 'package:flutter/foundation.dart';
import 'login.dart';
import 'dart:core';


class Welcome extends StatelessWidget {

  static const routeName = '/welcome-screen';

  const Welcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      'https://kinsta.com/wp-content/uploads/2019/03/create-a-qr-code.png'),
                  fit: BoxFit.cover),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 6,
                child: Padding(
                  padding: const EdgeInsets.only(top: 60, left: 25),
                  child:
                  SizedBox(
                    width: 250.0,
                    child: DefaultTextStyle(
                      style: const TextStyle(
                        fontSize: 35,
                        color: Colors.white,

                      ),
                      child: Center(
                        child: AnimatedTextKit(
                          repeatForever: true,
                          animatedTexts: [
                            FlickerAnimatedText('welcome'),
                            FlickerAnimatedText('to'),
                            RotateAnimatedText('Qr Access'),
                          ],
                          onTap: () {
                            if (kDebugMode) {
                              print("Welcome");
                            }
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                 child: Column(
                  children: [
                    Container(
                      height: 80,
                      width: double.infinity,
                      padding: const EdgeInsets.only(
                          top: 25, left: 24, right: 24),
                      child: TextButton(
                        child: Text('Log In',style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,)),
                        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.indigo),
                      ),
                        onPressed: () {Navigator.of(context).pushNamed(
                          LoginScreen.routeName);  },
                    ),
                    ),
                    Container(
                      height: 80,
                      width: double.infinity,
                      padding: const EdgeInsets.only(
                          top: 25, left: 24, right: 24),
                      child: TextButton(
                        child: Text('Sign Up',style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.lightBlue,)),
                        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white),
                        ),
                        onPressed: () {Navigator.of(context).pushNamed(
                            SignupScreen.routeName);  },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
