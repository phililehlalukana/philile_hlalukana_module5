import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:philile_hlalukana_module5/screens/dashboard2.dart';
import 'package:philile_hlalukana_module5/screens/home_chillers.dart';
import 'package:philile_hlalukana_module5/screens/login.dart';
import 'package:philile_hlalukana_module5/screens/register.dart';
import 'package:philile_hlalukana_module5/screens/user_profile_edit.dart';
import 'package:philile_hlalukana_module5/screens/welcome.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';



void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyQrAppHome());
}

class MyQrAppHome extends StatelessWidget {
  const MyQrAppHome({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    return MaterialApp(home: const SplashScreen(),

      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColorLight: Colors.blue,
        primaryColorDark: Colors.blueGrey,

      ),
      routes: {
        Welcome.routeName: (context) =>  const Welcome(),
        SignupScreen.routeName: (context) => SignupScreen(),
        LoginScreen.routeName: (context) => LoginScreen(),
        ProfilePage.routeName: (context) =>   const ProfilePage(),
        HomeChillersPage.routeName: (context) =>  const HomeChillersPage(),
        Dashboardd.routeName: (context) => const Dashboardd(),
        EditProfilePage.routeName: (context) => const EditProfilePage(),



      },
    );
  }
}

class SplashScreen extends StatefulWidget {
   const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed( const Duration(seconds:9), () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const Welcome(),
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      Center(
        child: Row(

          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              width: 300.0,
              height: 300.0,
              child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0Czw7FRmiGiIqKr03FXwBU0ihxmGUpVc-Wat2-ENju7LKKU5JbshaeHRDtLmzOg9JiuY&usqp=CAU"),

            ),


             const SizedBox(width: 20.0, height: 100.0),
             const Text(
              'QR',
              style: TextStyle(fontSize: 40.0,fontFamily: 'Horizon', color:Colors.blue),

            ),
             const SizedBox(width: 20.0, height: 100.0),
            DefaultTextStyle(
              style: const TextStyle(
                fontSize: 40.0,
                fontFamily: 'Horizon',
              ),
              child: Center(
                child: AnimatedTextKit(
                  animatedTexts: [
                    RotateAnimatedText('GIVES'),
                    RotateAnimatedText('YOU'),
                    RotateAnimatedText('ACCESS'),

                  ],
                ),
              ),
            ),
          ],
        ),

      );

  }
}



