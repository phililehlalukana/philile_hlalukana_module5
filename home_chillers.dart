import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';




class HomeChillersPage extends StatefulWidget {
  static const routeName = '/homechillerspage';
  const HomeChillersPage({Key? key}) : super(key: key);

  @override
  _HomeChillersPageState createState() => _HomeChillersPageState();
}

 class _HomeChillersPageState extends State<HomeChillersPage> {

  final listOfPackages = ["Home Chillers Basic",

    "Home Chillers Access ",
    "Home Chillers Premium"];
  String? dropdownValue = 'Home Chillers Basic';


  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController ageController = TextEditingController();
  final CollectionReference subscriptions  = FirebaseFirestore.instance.collection('subscriptions');



  Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      nameController.text = documentSnapshot['name'];
      emailController.text = documentSnapshot['email'];
      dropdownValue = documentSnapshot['subscription'].toString();
      ageController.text = documentSnapshot['age'].toString();
    }

    await showModalBottomSheet(

        isScrollControlled: true,

        context: context,
        builder: (BuildContext ctx) {

          return Padding(

            padding: EdgeInsets.only(

                top: 20,
                left: 20,
                right: 20,
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20,


          ),

            child: Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: "Enter Your Name",
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),

                ),
                  const SizedBox(height: 8.0),

                  TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
                      labelText: "Enter Your Email",
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),

                  ),
                  const SizedBox(height: 8.0),

                  DropdownButtonFormField(
                    value: dropdownValue,
                    icon: const Icon(Icons.arrow_downward),
                    decoration: InputDecoration(
                      labelText: "Select Subscription Type",
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    items: listOfPackages.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      }
                      );
                      },
                  ),
                  const SizedBox(height: 8.0),

                  TextFormField(
                    keyboardType: TextInputType.number,
                    controller: ageController,
                    decoration: InputDecoration(
                      labelText: "Enter Your Age",
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),


                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          action == 'create' ? 'Create' : 'Update'),
                    ),
                    onPressed: () async {
                      final String? name = nameController.text;
                      final String? email = emailController.text;
                      final String? subscription = dropdownValue;
                      final double? age =
                      double.tryParse(ageController.text);
                      if (name != null && subscription != null &&
                          email != null && age != null) {
                        if (action == 'create') {
                          
                          await subscriptions.add({
                            "name": name,
                            "subscription": subscription,
                            "email": email,
                            "age": age,
                          });
                        }

                        if (action == 'update') {
                         
                          await subscriptions
                              .doc(documentSnapshot!.id)
                              .update({
                            "name": name,
                            "subscription": subscription,
                            "email": email,
                            "age": age,
                          });
                        }

                        nameController.text = '';
                        emailController.text = '';
                        ageController.text = '';
                        dropdownValue = '';

                        Navigator.of(context).pop();
                      }
                    },
                  )
                ],
              ),
            ),
          );
        }
        );
  }




  Future<void> _deleteProduct(String productId) async {
    await subscriptions.doc(productId).delete();



    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a Package')));

  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: const Text('Home Chillers'),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(
                  'https://media.istockphoto.com/videos/asian-woman-having-virtual-party-in-vr-glasses-at-home-video-id1316991031?s=640x640'),
              fit: BoxFit.cover),
        ),
              child: StreamBuilder (
         stream: subscriptions.snapshots(),
         builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
           if (streamSnapshot.hasData) {
             return ListView.builder(
               itemCount: streamSnapshot.data!.docs.length,
               itemBuilder: (context, index) {
                 final DocumentSnapshot documentSnapshot =
                 streamSnapshot.data!.docs[index];
                 return Card(
                     color: Colors.white.withOpacity(0.5),
                   margin: const EdgeInsets.all(10),

                   child: ListTile(
                     title: Text("Hi! ${ documentSnapshot['name']}."),
                     subtitle: Text("You are currently a subscriber to ${documentSnapshot['subscription']}package"),
                     trailing: SizedBox(
                       width: 100,
                       child: Row(
                         children: [
                           
                           IconButton(
                               icon: const Icon(Icons.edit),
                               onPressed: () =>
                                   _createOrUpdate(documentSnapshot)),
                           
                           IconButton(
                               icon: const Icon(Icons.delete),
                               onPressed: () =>
                                   _deleteProduct(documentSnapshot.id)),
                         ],
                       ),
                     ),
                   ),
                 );
               },
             );
           }

           return const Center(
             child: CircularProgressIndicator(),
           );
         },
       ),
      


  ),
        floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
    child: const Icon(Icons.add),
    ),
    );
}

}

